package simplecalculatortest;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import simplecalculator.SimpleCalculator;

public class SimpleCalculatorTest {
  SimpleCalculator simpleCalculator;

  @BeforeTest
  public void setup(){
    simpleCalculator = new SimpleCalculator();
  }

  @Test(priority = 0)
  public void testPenjumlahan(){
    float result = simpleCalculator.penjumlahan(10, 10);
    System.out.println("Hasil penjumlahan " + result);
    Assert.assertEquals(result, 20);
  }

  @Test(priority = 1)
  public void testPengurangan(){
    float result = simpleCalculator.pengurangan(50, 10);
    System.out.println("Hasil pengurangan " + result);
    Assert.assertEquals(result, 40);
  }

  @Test(priority = 2)
  public void testPerkalian(){
    float result = simpleCalculator.perkalian(5, 10);
    System.out.println("Hasil perkalian " + result);
    Assert.assertEquals(result, 50);
  }

  @Test(priority = 3)
  public void testPembagian(){
    float result = simpleCalculator.pembagian(50, 10);
    System.out.println("Hasil pembagian " + result);
    Assert.assertEquals(result, 5);
  }

  @Test(priority = 4)
  public void testModulus(){
    float result = simpleCalculator.modulus(77, 10);
    System.out.println("Hasil modulus " + result);
    Assert.assertEquals(result, 7);
  }
}
