package simplecalculator;

public class SimpleCalculator {
  public float penjumlahan(float a, float b){
    return a + b;
  }

  public float pengurangan(float a, float b){
    return a - b;
  }

  public float perkalian(float a, float b){
    return a * b;
  }

  public float pembagian(float a, float b){
    return a / b;
  }

  public float modulus(float a, float b){
    return a % b;
  }
}
